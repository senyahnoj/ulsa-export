# ULSA archive site exporter

This project is to convert a legacy content from a bespoke website into a wordpress import (RSS + extensions). Look at the xml file in the docs/ folder for an example.

## Building

You need to have composer installed - see https://getcomposer.org and PHP 7.2 or above.

In the root of the project run:

```
composer create-project
```

You also need to copy config.php-example to config.php and put your database connection details in (you will need a copy of the ulsa database).

## Running

Running the export script on the command line in the bin directory should read the database and export to wordpress RSS format.
