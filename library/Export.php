<?php
namespace UlsaExport;

use JonDb\Db;
use JonDb\Results;
use SimpleXMLElement;
use Carbon\Carbon;

class Export
{
    /**
     * @var Results query results instance
     */
    protected $query;

    /**
     * @var SimpleXMLElement RSS
     */
    protected $xml;

    /**
     * @var SimpleXMLElement RSS channel
     */
    protected $channel;

    /**
     * @var string URL prefix
     */
    protected $url = 'https://wp.ulsa.org.uk';

    /**
     * @const WP namespace
     */
    const NS_WP = 'http://wordpress.org/export/1.2/';

    /**
     * @const all database times are in Europe/London timezone
     */
    const TZ = 'Europe/London';

    /**
     * Instantiates DB connection etc.
     */
    public function __construct()
    {
        $this->connect();
        $this->xml = new SimpleXMLElement('<rss version="2.0"/>');
        $this->channel = $this->xml->addChild('channel');
    }

    /**
     * Exports the rantbook to RSS
     *
     * @return
     */
    public function rantbook()
    {
        $sql = 'SELECT * FROM rb_rant';
        $rants = $this->query->getAll($sql);

        foreach ($rants as $rant) {
            $this->formatRant($rant);
        }
        $dom = dom_import_simplexml($this->xml)->ownerDocument;
        $dom->formatOutput = true;
        echo $dom->saveXML();
    }

    /**
     * Convert ISO-8859-1 encoding into UTF-8 with html escaping
     *
     * @param string $string string
     * @return string
     */
    protected function enc($string)
    {
        $string = str_replace("\r", '', $string);
        $string = utf8_encode($string);
        $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
        return $string;
    }

    /**
     * Formats the given rant
     *
     * @param array $rant rant record
     * @return void
     */
    protected function formatRant(array $rant)
    {
        $item = $this->channel->addChild('item');
        $item->addChild('title', $this->enc($rant['rant_title']));
        $item->addChild('link', $this->makeLink($rant));
        $date = Carbon::parse($rant['posted'], self::TZ);
        $item->addChild('pubDate', $date->format('r'));
        $item->addChild('content', $this->makeContent($rant)); // TODO

        // Wordpress namespaced meta information
        $item->addChild('post_date', $date->format('Y-m-d H:i:s'), self::NS_WP);
        $item->addChild('post_date_gmt', gmdate('Y-m-d H:i:s', $date->format('U')), self::NS_WP);
        $item->addChild('status', '<!CDATA[publish]>', self::NS_WP); // TODO
    }

    /**
     * Makes the link based on the rant content, dates etc.
     *
     * @param array $rant rant record
     * @return string
     */
    protected function makeLink(array $rant)
    {
        $date = new \DateTime($rant['posted']);
        return sprintf(
            '%s/index.php/%d/%s/%s/%s/',
            $this->url,
            $date->format('Y'),
            $date->format('m'),
            $date->format('d'),
            $this->slug($rant['rant_title'])
        );
    }

    /**
     * Makes a URL slug for the given title
     *
     * @param string $title title
     * @return string
     */
    protected function slug($title)
    {
        $slug = new UrlSlug();
        return $slug->urlSlug(utf8_encode($title));
    }

    /**
     * Format rant into content
     *
     * @param array $rant rant record
     * @return string
     */
    protected function makeContent(array $rant)
    {
        $text = $this->enc($rant['rant_text']);
        if ($rant['html'] === 'y') {
            return $text;
        } else {
            // todo convert the markup to HTML
            return 'todo - converted markup to HTML';
        }
    }

    /**
     * Connect to the database
     *
     * @return void
     */
    protected function connect()
    {
        Db::getInstance()->connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        $this->query = new Results();
    }

}
